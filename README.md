# Pocketbase

A container including the pocketbase executable.

## Usage

### Docker Run
```shell
docker run --rm -d 
    -p 8090:8090
    -v /path/to/data:/pb_data
    -v /path/to/public:/pb_public # Optional
    djdietrick/pocketbase
```

### Docker compose
```yaml
services:
    pocketbase:
        image: djdietrick/pocketbase
        restart: unless-stopped
        ports:
            - "8090:8090"
        volumes:
            - /path/to/data:/pb_data
            - /path/to_public:/pb_public # Optional
        healthcheck: #optional (recommended) since v0.10.0
            test: wget --no-verbose --tries=1 --spider http://localhost:8090/api/health || exit 1
            interval: 5s
            timeout: 5s
            retries: 5
```

## Building

```shell
docker build -t <username>/<repo_name> .
# To push
docker push <username>/<repo_name>
```