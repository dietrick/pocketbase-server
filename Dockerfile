FROM amd64/ubuntu

WORKDIR /app

RUN apt-get update
RUN apt-get install -y unzip curl sendmail && \
    curl -L -o pocketbase.zip https://github.com/pocketbase/pocketbase/releases/download/v0.10.2/pocketbase_0.10.2_linux_amd64.zip && \
    unzip pocketbase.zip && \
    rm pocketbase.zip && \
    chmod +x pocketbase

EXPOSE 8090

ENTRYPOINT ["/app/pocketbase", "serve", "--http", "0.0.0.0:8090"]
